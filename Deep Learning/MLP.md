# MLP
## Network properties 
* data: MNIST, 60K TRAINING, 10K TESTING
* Minibatch training {1, 256, 60K) = (online, minibatch, batch)
* RMSP Optimizer learning rate = 1e-3, rho in {0.01, 0.99}
* L2-Reg alpha in {0.01, 0.1, 1}
* L1-Norm alpha = 0.01, dropout = 0.3
* Layer 1 128 neurons, Layer 2 256 neurons
* final activation function -> softmax
* loss function: categorical cross entropy
* metric -> accuracy 
* 100 epochs
* 20% of training for validation


|   |   |   |   |   |
|---|---|---|---|---|
|   |   |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |

## TO-DO
* Training speed for 3 different training methods
* Accuracy and cost curves for training and validation
* Comment on the curves
* Overfitting, underfitting and model efficiency


# MLP TUNING
* RMSP Optimizer lr = 1e-3, rho in {0.01, 0.99}
* L2-Reg
* He-normal weight initialization
* 2 Layers
* Grid:
	* Layer 1 neurons {64, 128}
	* Layer 2 neurons {256, 512}
	* L2 alpha {1e-1, 1e-2, 1e-6}
	* learning rate {1e-1, 1e-2, 1e-3}
* Grid search and 5-fold or keras tuner
* Early stopping, patience 200
* ReLU HL, Softmax OL
* F-measure metric
* 20% validation, no of epochs 1000

* *``